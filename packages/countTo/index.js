import CountTo from "./src"
CountTo.install = Vue => {
    Vue.component(CountTo.name, CountTo)
}
export default CountTo