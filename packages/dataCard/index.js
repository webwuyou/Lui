import DataCard from "./src"
DataCard.install = Vue => {
    Vue.component(DataCard.name, DataCard)
}

export default DataCard