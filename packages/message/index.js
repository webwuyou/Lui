import Message from "./src"
// import Vue from "vue"
Message.install = Vue => {
    Vue.component(Message.name, Message)
}

export default Message