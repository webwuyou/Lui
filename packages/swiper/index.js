import Swiper from "./src"
Swiper.install = Vue => {
    Vue.component(Swiper.name, Swiper)
}

export default Swiper