import Button from "./button"
import Message from "./message"
import Swiper from "./swiper"
import Row from "./row"
import Col from "./col"
import CountTo from "./countTo"
import DataCard from "./dataCard"
import FullScreen from "./fullScreen"

// 全局引入，所有的组件都需要注册到Vue component上，并导出
let components = [Button, Message, Swiper, Row, Col, CountTo, DataCard, FullScreen]
    // 定义install方法，接收Vue作为参数，如果使用use注册的话，则所有组件都将被注册
const install = Vue => {
    // 判断是否安装
    if (install.installed) return
        // 遍历注册全局组件
    components.map(component => Vue.component(component.name, component))
        // 下面这个方法也可以
        // components.map(component=>Vue.use(component))
    Vue.prototype.$message = Message
}

// 判断是否是直接引入文件（检测到vue才执行）
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}

// 按需引入，导出单个组件即可
export default {
    // 导出的对象必须具有install，才能别Vue.use()方法安装
    install,
    // 以下是具体的组件列表
    ...components,
    // Button,
    // Message
}