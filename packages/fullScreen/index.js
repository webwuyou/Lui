import FullScreen from "./src"
FullScreen.install = Vue => {
    Vue.component(FullScreen.name, FullScreen)
}