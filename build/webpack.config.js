const path = require("path")
const webpack = require("webpack")
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require("vue-loader/lib/plugin")
module.exports = {
  entry: './packages/index.js',
  output: {
    path: path.resolve(__dirname, '../lib'),
    publicPath: '/lib/',
    filename: 'index.js',//生成的文件名
    library: 'mychajian',//指定使用require引入时的模块名
    libraryTarget: 'umd',//输出格式 umd同时支持在node环境和浏览器环境执行
    umdNamedDefine: true,//否将模块名称作为 AMD 输出的命名空间

  },
  // 组件库的使用者在使用的时候会自行导入vue，这里就不应该将vue打包进组件库
  // externals: {
  //   vue: 'vue'
  // },
  externals: {
    // 为各种导入形式设置不同名称
    vue: {
      root: 'Vue',
      commonjs: 'vue',
      commonjs2: 'vue',
      amd: 'vue'
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'scss-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|ttf|svg|woff|eot)$/,
        loader: 'url-loader',
        query: {
          limit: 30000//把小于30k的图片打包成base64
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new UglifyJsPlugin({
      uglifyOptions: {
        compress: {
          drop_console: true, // 删除所有的 `console` 语句，可以兼容ie浏览器
          collapse_vars: true,    // 内嵌定义了但是只用到一次的变量
          reduce_vars: true  // 提取出出现多次但是没有定义成变量去引用的静态值
        },
        output: {
          comments: false,
          beautify: false
        }
      }
    })
  ]
}